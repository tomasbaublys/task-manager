'use strict';

/*************************************
	Variables init
**************************************/


/*************************************
		Main functions
**************************************/

function addItem(){

	var firstInputValue = $('#list-to-add li:first-child input').val();

 	if(firstInputValue != '') {

		var li = "<li>" + 
		 				"<i class='remove fa fa-times icon'></i>" +
		 				"<input type='text' class='text' autocomplete='off'>" +
		 				"<span class='selector'></span>" +
	 				"</li>";

	 	$('#list-to-add').prepend($(li));
	 	$('#list-to-add li:first-child input').focus();

	 	$('#list-to-add li:first-child .remove').on('click', function(){

			$(this).parent().remove();
		
	 	});

	 	$('#list-to-add li:first-child .selector').on('click', function(){

	 		$(this).parent().toggleClass('done');

	 	});

	}
}

/*************************************
			Boot
**************************************/

$(document).ready(function(){

	$(document).on('keydown', function(event){

		var keyCode = event.keyCode;

		if(keyCode == 13) {

			addItem();
		}

	});

		console.log
 	
 	$('#add').on('click', function(){

 		addItem();

 	});

 	$('.selector').on('click', function(){

 		$(this).parent().toggleClass('done');

 	});

 	$('.remove').on('click', function(){

	$(this).parent().remove();

 	});

});